package com.mcos.laras.bahasaarab;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.TextView;

public class About extends AppCompatActivity {

    TextView tv_about_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.about);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        tv_about_title = (TextView) findViewById(R.id.tv_about_title);
        Typeface tf_title = Typeface.createFromAsset(getAssets(), "coffe_with_sugar.ttf");
        tv_about_title.setTypeface(tf_title);

    }
}