package com.mcos.laras.bahasaarab;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.util.Log;

/**
 * Created by kopral on 11/26/16.
 */

public class AudioPlayerHelper {

    MediaPlayer mediaPlayer = null;
    Context cont;

    AudioPlayerHelper(Context val_cont) {
        this.cont = val_cont;
    }

    public void playSound(final String fileName) {
        mediaPlayer = new MediaPlayer();
        //Log.d("Audio",fileName.toLowerCase());
        try {
            AssetFileDescriptor afd = cont.getAssets().openFd(fileName.toLowerCase() + ".mp3");
            mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            afd.close();
            mediaPlayer.prepare();
        } catch (final Exception e) {
            e.printStackTrace();
        }

        mediaPlayer.start();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                mediaPlayer.stop();
                mediaPlayer.reset();
                mediaPlayer.release();
            }
        });
    }

    public void playSoundFX(final String fileName) {
        mediaPlayer = new MediaPlayer();

        try {
            AssetFileDescriptor afd = cont.getAssets().openFd(fileName + ".wav");
            mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            afd.close();
            mediaPlayer.prepare();
        } catch (final Exception e) {
            e.printStackTrace();
        }

        mediaPlayer.start();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                mediaPlayer.stop();
                mediaPlayer.reset();
                mediaPlayer.release();
            }
        });
    }

}
