package com.mcos.laras.bahasaarab;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;

public class Latihan extends AppCompatActivity {

    AudioPlayerHelper adh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.latihan);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        adh = new AudioPlayerHelper(this);

    }

    public void LatihanClick(View v) {
        adh.playSoundFX("fx_music_marimba_chord");
        Intent itn;
        switch (v.getId()) {
            case R.id.linLay_latihan_tombol_buah:
                itn = new Intent(Latihan.this, LatihanDetail.class);
                itn.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                itn.putExtra("category", "buah");
                startActivity(itn);
                break;
            case R.id.linLay_latihan_tombol_hewan:
                itn = new Intent(Latihan.this, LatihanDetail.class);
                itn.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                itn.putExtra("category", "hewan");
                startActivity(itn);
                break;
            case R.id.linLay_latihan_tombol_warna:
                itn = new Intent(Latihan.this, LatihanDetail.class);
                itn.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                itn.putExtra("category", "warna");
                startActivity(itn);
                break;
            case R.id.linLay_latihan_tombol_angka:
                itn = new Intent(Latihan.this, LatihanDetail.class);
                itn.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                itn.putExtra("category", "angka");
                startActivity(itn);
                break;
            case R.id.linLay_latihan_tombol_hari:
                itn = new Intent(Latihan.this, LatihanDetail.class);
                itn.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                itn.putExtra("category", "hari");
                startActivity(itn);
                break;
            case R.id.linLay_latihan_tombol_anggotatubuh:
                itn = new Intent(Latihan.this, LatihanDetail.class);
                itn.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                itn.putExtra("category", "anggota_tubuh");
                startActivity(itn);
                break;
        }
    }
}