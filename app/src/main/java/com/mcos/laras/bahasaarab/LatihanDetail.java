package com.mcos.laras.bahasaarab;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

public class LatihanDetail extends AppCompatActivity {
    String category;
    ArrayList<HashMap<String, String>> list_latihan;
    ArrayList<Integer> list_pilihan_ganda;
    DBHelper db;
    AudioPlayerHelper aph;
    TextView tv_latihandetail_soal;

    TextView tv_latihandetail_title_pil_1;
    TextView tv_latihandetail_title_pil_2;
    TextView tv_latihandetail_title_pil_3;
    TextView tv_latihandetail_title_pil_4;

    ImageView iv_latihandetail_image;

   /* TextView tv_latihandetail_caption_pil_1;
    TextView tv_latihandetail_caption_pil_2;
    TextView tv_latihandetail_caption_pil_3;
    TextView tv_latihandetail_caption_pil_4;*/

    RadioGroup rg_latihandetail_container;
    RadioButton rb_latihandetail_pil_1;
    RadioButton rb_latihandetail_pil_2;
    RadioButton rb_latihandetail_pil_3;
    RadioButton rb_latihandetail_pil_4;

    int navigation_pos = 0;

    int benar = 0;
    int salah = 0;
    int jawaban = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.latihandetail);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();


        category = getIntent().getStringExtra("category");

        db = new DBHelper(getBaseContext());
        aph = new AudioPlayerHelper(LatihanDetail.this);

        list_latihan = db.getMateriByCategory(category);
        list_pilihan_ganda = new ArrayList<Integer>();

        tv_latihandetail_soal = (TextView) findViewById(R.id.tv_latihandetail_soal);

        tv_latihandetail_title_pil_1 = (TextView) findViewById(R.id.tv_latihandetail_title_pil_1);
        tv_latihandetail_title_pil_2 = (TextView) findViewById(R.id.tv_latihandetail_title_pil_2);
        tv_latihandetail_title_pil_3 = (TextView) findViewById(R.id.tv_latihandetail_title_pil_3);
        tv_latihandetail_title_pil_4 = (TextView) findViewById(R.id.tv_latihandetail_title_pil_4);
        iv_latihandetail_image = (ImageView) findViewById(R.id.iv_latihandetail_image);
     /*   tv_latihandetail_caption_pil_1 = (TextView) findViewById(R.id.tv_latihandetail_caption_pil_1);
        tv_latihandetail_caption_pil_2 = (TextView) findViewById(R.id.tv_latihandetail_caption_pil_2);
        tv_latihandetail_caption_pil_3 = (TextView) findViewById(R.id.tv_latihandetail_caption_pil_3);
        tv_latihandetail_caption_pil_4 = (TextView) findViewById(R.id.tv_latihandetail_caption_pil_4);*/

        rb_latihandetail_pil_1 = (RadioButton) findViewById(R.id.rb_latihandetail_pil_1);
        rb_latihandetail_pil_2 = (RadioButton) findViewById(R.id.rb_latihandetail_pil_2);
        rb_latihandetail_pil_3 = (RadioButton) findViewById(R.id.rb_latihandetail_pil_3);
        rb_latihandetail_pil_4 = (RadioButton) findViewById(R.id.rb_latihandetail_pil_4);

        //mengacak urutan soal
        long seed = System.nanoTime();
        Log.d("Randoms", "before: " + list_latihan.toString());
        Collections.shuffle(list_latihan, new Random(seed));
        Log.d("Randoms", "after: " + list_latihan.toString());
        iv_latihandetail_image.getViewTreeObserver().addOnPreDrawListener(
                new ViewTreeObserver.OnPreDrawListener() {
                    public boolean onPreDraw() {
                        int finalHeight = iv_latihandetail_image.getMeasuredHeight();
                        int finalWidth = iv_latihandetail_image.getMeasuredWidth();

                        iv_latihandetail_image.getLayoutParams().height = finalWidth;

                        Log.d("param width", String.valueOf(finalWidth));
                        Log.d("param height", String.valueOf(finalHeight));

                        return true;
                    }
                });
        //set soal
        setSoal();

        Log.d("nav", String.valueOf(navigation_pos));
    }

    public void PilihJawaban(View v) {
        aph.playSoundFX("fx_pop_drip");
        jawaban = -1;
        switch (v.getId()) {
            case R.id.rb_latihandetail_pil_1:
                rb_latihandetail_pil_1.setChecked(true);
                rb_latihandetail_pil_2.setChecked(false);
                rb_latihandetail_pil_3.setChecked(false);
                rb_latihandetail_pil_4.setChecked(false);
                jawaban = list_pilihan_ganda.get(0);
                break;
            case R.id.rb_latihandetail_pil_2:
                rb_latihandetail_pil_1.setChecked(false);
                rb_latihandetail_pil_2.setChecked(true);
                rb_latihandetail_pil_3.setChecked(false);
                rb_latihandetail_pil_4.setChecked(false);
                jawaban = list_pilihan_ganda.get(1);
                break;
            case R.id.rb_latihandetail_pil_3:
                rb_latihandetail_pil_1.setChecked(false);
                rb_latihandetail_pil_2.setChecked(false);
                rb_latihandetail_pil_3.setChecked(true);
                rb_latihandetail_pil_4.setChecked(false);
                jawaban = list_pilihan_ganda.get(2);
                break;
            case R.id.rb_latihandetail_pil_4:
                rb_latihandetail_pil_1.setChecked(false);
                rb_latihandetail_pil_2.setChecked(false);
                rb_latihandetail_pil_3.setChecked(false);
                rb_latihandetail_pil_4.setChecked(true);
                jawaban = list_pilihan_ganda.get(3);
                break;
        }
    }

    public void LatihanDetailClick(View v) {
        switch (v.getId()) {
            case R.id.btn_latihandetail_next:
                if (jawaban != -1) {
                    if (list_latihan.get(navigation_pos).get("arab").equalsIgnoreCase(list_latihan.get(jawaban).get("arab"))) {
                        aph.playSoundFX("fx_chime_bell_ding");
                        benar++;
                    } else {
                        aph.playSound("fx_error");
                        salah++;
                    }

                    if (navigation_pos < list_latihan.size() - 1) {
                        navigation_pos++;
                        setSoal();
                        Log.d("nav", String.valueOf(navigation_pos));
                    } else {
                        Intent itn = new Intent(LatihanDetail.this, Result.class);
                        itn.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        itn.putExtra("benar", benar);
                        itn.putExtra("salah", salah);
                        startActivity(itn);
                    }
                } else {
                    Toast.makeText(LatihanDetail.this, "Anda belum memilih jawaban", Toast.LENGTH_SHORT).show();
                }
                jawaban = -1;
                break;
            case R.id.btn_latihandetail_sound_pil_1:
                aph.playSound(list_latihan.get(list_pilihan_ganda.get(0)).get("indonesia"));
                break;
            case R.id.btn_latihandetail_sound_pil_2:
                aph.playSound(list_latihan.get(list_pilihan_ganda.get(1)).get("indonesia"));
                break;
            case R.id.btn_latihandetail_sound_pil_3:
                aph.playSound(list_latihan.get(list_pilihan_ganda.get(2)).get("indonesia"));
                break;
            case R.id.btn_latihandetail_sound_pil_4:
                aph.playSound(list_latihan.get(list_pilihan_ganda.get(3)).get("indonesia"));
                break;
        }

        //hilangkan check
        rb_latihandetail_pil_1.setChecked(false);
        rb_latihandetail_pil_2.setChecked(false);
        rb_latihandetail_pil_3.setChecked(false);
        rb_latihandetail_pil_4.setChecked(false);
    }

    private void setSoal() {
        //set soal
        tv_latihandetail_soal.setText(list_latihan.get(navigation_pos).get("indonesia"));

        int imageId = getResources().getIdentifier(list_latihan.get(navigation_pos).get("indonesia").toLowerCase().replaceAll("\\s", ""), "mipmap", getPackageName());
        iv_latihandetail_image.setImageResource(imageId);

        list_pilihan_ganda = new ArrayList<Integer>();
        Log.d("Randoms", "========");

        //menentukan pilihan ganda
        Random rand = new Random();
        long seed2 = System.nanoTime();
        for (int i = 0; i < 4; i++) {
            if (i == 0) {
                list_pilihan_ganda.add(navigation_pos);
            } else {
                int randomNum = rand.nextInt(((list_latihan.size() - 1) - 1) + 1) + 1;
                Log.d("Randoms", "pilihan ganda: " + list_pilihan_ganda.toString() + ", RandomNums: " + randomNum + ", Navigation pos: " + navigation_pos + ", contains? " + list_pilihan_ganda.contains(randomNum));
                if (list_pilihan_ganda.contains(randomNum)) {
                    while (list_pilihan_ganda.contains(randomNum)) {
                        randomNum = rand.nextInt(((list_latihan.size() - 1) - 1) + 1) + 1;
                        Log.d("Randoms", "Pilihan Ganda with Loop: " + randomNum);
                    }
                    list_pilihan_ganda.add(randomNum);
                } else {
                    list_pilihan_ganda.add(randomNum);
                }

            }
        }

        Collections.shuffle(list_pilihan_ganda, new Random(seed2));
        Log.d("Randoms", "Pilihan Ganda: " + list_pilihan_ganda.toString());

        //set pilihan ganda
        tv_latihandetail_title_pil_1.setText(list_latihan.get(list_pilihan_ganda.get(0)).get("arab").toString());
        tv_latihandetail_title_pil_2.setText(list_latihan.get(list_pilihan_ganda.get(1)).get("arab").toString());
        tv_latihandetail_title_pil_3.setText(list_latihan.get(list_pilihan_ganda.get(2)).get("arab").toString());
        tv_latihandetail_title_pil_4.setText(list_latihan.get(list_pilihan_ganda.get(3)).get("arab").toString());

        //settext title radio
/*        rb_latihandetail_pil_1.setText(list_latihan.get(list_pilihan_ganda.get(0)).get("arab").toString());
        rb_latihandetail_pil_2.setText(list_latihan.get(list_pilihan_ganda.get(1)).get("arab").toString());
        rb_latihandetail_pil_3.setText(list_latihan.get(list_pilihan_ganda.get(2)).get("arab").toString());
        rb_latihandetail_pil_4.setText(list_latihan.get(list_pilihan_ganda.get(3)).get("arab").toString());*/

    /*    tv_latihandetail_caption_pil_1.setText(list_latihan.get(list_pilihan_ganda.get(0)).get("arab").toString());
        tv_latihandetail_caption_pil_2.setText(list_latihan.get(list_pilihan_ganda.get(1)).get("arab").toString());
        tv_latihandetail_caption_pil_3.setText(list_latihan.get(list_pilihan_ganda.get(2)).get("arab").toString());
        tv_latihandetail_caption_pil_4.setText(list_latihan.get(list_pilihan_ganda.get(3)).get("arab").toString());*/
    }

}