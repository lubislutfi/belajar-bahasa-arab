package com.mcos.laras.bahasaarab;

import android.content.Intent;
import android.database.SQLException;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    DBHelper myDbHelper;

    TextView tv_main_title;
    ImageView iv_main_bg_awan1;
    ImageView iv_main_bg_awan2;
    ImageView iv_main_bg_matahari;
    ImageView iv_main_bg_pesawat;


    AudioPlayerHelper adh;

    // Animation
    Animation animAwan1;
    Animation animAwan2;
    Animation animPesawat;
    Animation animMatahari;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
        myDbHelper = new DBHelper(this);
        adh = new AudioPlayerHelper(this);
        tv_main_title = (TextView) findViewById(R.id.tv_main_title);
        Typeface tf_title = Typeface.createFromAsset(getAssets(), "coffe_with_sugar.ttf");
        tv_main_title.setTypeface(tf_title);

        iv_main_bg_awan1 = (ImageView) findViewById(R.id.iv_main_bg_awan1);
        iv_main_bg_awan2 = (ImageView) findViewById(R.id.iv_main_bg_awan2);
        iv_main_bg_matahari = (ImageView) findViewById(R.id.iv_main_bg_matahari);
        iv_main_bg_pesawat = (ImageView) findViewById(R.id.iv_main_bg_pesawat);

        // load the animation
        animAwan1 = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.awan1);
        animAwan2 = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.awan2);
        animPesawat = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.pesawat);
        animMatahari = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.matahari);

        iv_main_bg_awan1.startAnimation(animAwan1);
        iv_main_bg_awan2.startAnimation(animAwan2);
        iv_main_bg_pesawat.startAnimation(animPesawat);
        iv_main_bg_matahari.startAnimation(animMatahari);

        //int min = 0,max=10;
        Integer[] arr = new Integer[10];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = i;
        }

        Collections.shuffle(Arrays.asList(arr));
        //Log.d("Randoms", Arrays.toString(arr));

        try {
            myDbHelper.createDataBase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }

        try {
            myDbHelper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }
    }

    public void MainMenuClick(View v) {
        adh.playSoundFX("fx_music_marimba_chord");
        Intent itn;
        switch (v.getId()) {
            case R.id.btn_main_materi:
                itn = new Intent(MainActivity.this, Materi.class);
                startActivity(itn);
                break;
            case R.id.btn_main_latihan:
                itn = new Intent(MainActivity.this, Latihan.class);
                startActivity(itn);
                break;
            case R.id.linLay_main_container_tentang:
                itn = new Intent(MainActivity.this, About.class);
                startActivity(itn);
                break;
        }
    }
}