package com.mcos.laras.bahasaarab;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class MateriDetail extends AppCompatActivity {
    String category;
    ArrayList<HashMap<String, String>> list_materi;
    DBHelper db;
    TextView tv_materidetail_title;
    TextView tv_materidetail_caption;
    TextView tv_materidetail_ejaan;
    ImageView iv_materidetail_image;
    int navigation_pos = 0;

    AudioPlayerHelper aph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.materidetail);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        db = new DBHelper(getBaseContext());
        aph = new AudioPlayerHelper(MateriDetail.this);

        Log.d("Semua", db.getMateriByCategory("semua").toString());

        category = getIntent().getStringExtra("category");
        list_materi = db.getMateriByCategory(category);
        tv_materidetail_title = (TextView) findViewById(R.id.tv_materidetail_title);
        tv_materidetail_caption = (TextView) findViewById(R.id.tv_materidetail_caption);
        tv_materidetail_ejaan = (TextView) findViewById(R.id.tv_materidetail_ejaan);

        iv_materidetail_image = (ImageView) findViewById(R.id.iv_materidetail_image);

        tv_materidetail_title.setText(list_materi.get(navigation_pos).get("indonesia"));
        tv_materidetail_caption.setText(list_materi.get(navigation_pos).get("arab"));
        tv_materidetail_ejaan.setText(list_materi.get(navigation_pos).get("ejaan"));

        int imageId = getResources().getIdentifier(list_materi.get(navigation_pos).get("indonesia").toLowerCase().replaceAll("\\s", ""), "mipmap", getPackageName());
        iv_materidetail_image.setImageResource(imageId);

        iv_materidetail_image.getViewTreeObserver().addOnPreDrawListener(
                new ViewTreeObserver.OnPreDrawListener() {
                    public boolean onPreDraw() {
                        int finalHeight = iv_materidetail_image.getMeasuredHeight();
                        int finalWidth = iv_materidetail_image.getMeasuredWidth();
                        iv_materidetail_image.getLayoutParams().height = finalWidth;
                        Log.d("param width", String.valueOf(finalWidth));
                        Log.d("param height", String.valueOf(finalHeight));
                        return true;
                    }
                });

        Log.d("nav", String.valueOf(navigation_pos));
    }

    public void MateriDetailClick(View v) {

        switch (v.getId()) {
            case R.id.btn_materidetail_next:
                if (navigation_pos < list_materi.size() - 1) {
                    aph.playSoundFX("fx_chime_bell_ding");
                    navigation_pos++;
                    tv_materidetail_title.setText(list_materi.get(navigation_pos).get("indonesia"));
                    tv_materidetail_caption.setText(list_materi.get(navigation_pos).get("arab"));
                    tv_materidetail_ejaan.setText(list_materi.get(navigation_pos).get("ejaan"));

                    int imageId = getResources().getIdentifier(list_materi.get(navigation_pos).get("indonesia").toLowerCase().replaceAll("\\s", ""), "mipmap", getPackageName());
                    iv_materidetail_image.setImageResource(imageId);
                    Log.d("nav", String.valueOf(navigation_pos));
                } else {
                    aph.playSoundFX("fx_digi_plink");
                }
                break;
            case R.id.btn_materidetail_prev:
                if (navigation_pos > 0) {
                    aph.playSoundFX("fx_chime_bell_ding");
                    navigation_pos--;
                    tv_materidetail_title.setText(list_materi.get(navigation_pos).get("indonesia"));
                    tv_materidetail_caption.setText(list_materi.get(navigation_pos).get("arab"));
                    int imageId = getResources().getIdentifier(list_materi.get(navigation_pos).get("indonesia").toLowerCase(), "mipmap", getPackageName());
                    iv_materidetail_image.setImageResource(imageId);
                    Log.d("nav", String.valueOf(navigation_pos));
                } else {
                    aph.playSoundFX("fx_digi_plink");
                }
                break;
            case R.id.btn_materidetail_sound:
                aph.playSound(list_materi.get(navigation_pos).get("indonesia"));
                Log.d("Button", "materi detail sound clicked");
                break;
        }
    }
}