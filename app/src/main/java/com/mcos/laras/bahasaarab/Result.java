package com.mcos.laras.bahasaarab;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

public class Result extends AppCompatActivity {
    int benar;
    int salah;
    float nilai;

    TextView tv_result_benar;
    TextView tv_result_salah;
    TextView tv_result_nilai;
    AudioPlayerHelper aph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        aph = new AudioPlayerHelper(this);
        aph.playSound("fx_fiesta_trio");

        benar = getIntent().getIntExtra("benar", 0);
        salah = getIntent().getIntExtra("salah", 0);
        nilai = ((float) benar / (float) (benar + salah)) * (float) 100;

        tv_result_benar = (TextView) findViewById(R.id.tv_result_benar);
        tv_result_salah = (TextView) findViewById(R.id.tv_result_salah);
        tv_result_nilai = (TextView) findViewById(R.id.tv_result_nilai);

        tv_result_benar.setText(String.valueOf(benar));
        tv_result_salah.setText(String.valueOf(salah));
        tv_result_nilai.setText(String.valueOf((int) nilai));
    }

    public void ResultClick(View v) {
        aph.playSoundFX("fx_music_marimba_chord");
        Intent itn;
        switch (v.getId()) {
            case R.id.btn_result_ok:
                itn = new Intent(Result.this, MainActivity.class);
                itn.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(itn);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        aph.playSoundFX("fx_music_marimba_chord");
        Intent itn = new Intent(Result.this, MainActivity.class);
        itn.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(itn);
    }
}